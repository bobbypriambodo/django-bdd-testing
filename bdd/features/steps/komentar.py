from behave import *
from selenium.webdriver.common.keys import Keys
from lists.models import Item

@given('I am opening the web page')
def impl(context):
  context.browser.get(context.config.server_url)

@when('I see 0 item’s in my to do list')
def impl(context):
  table = context.browser.find_element_by_id('id_list_table')
  rows = table.find_elements_by_tag_name('tr')
  assert len(rows) == 0

@then('I see a “yey, waktunya berlibur” about how busy I am')
def impl(context):
  comment = context.browser.find_element_by_id('comment')
  assert comment.text == 'yey, waktunya berlibur'

@when('I see 3 item’s in my to do list')
def impl(context):
  for i in range(0, 3):
    inputbox = context.browser.find_element_by_id('id_new_item')
    inputbox.send_keys('test')
    inputbox.send_keys(Keys.ENTER)
  table = context.browser.find_element_by_id('id_list_table')
  rows = table.find_elements_by_tag_name('tr')
  assert len(rows) == 3

@then('I see a “sibuk tapi santai” about how busy I am')
def impl(context):
  comment = context.browser.find_element_by_id('comment')
  assert comment.text == 'sibuk tapi santai'

@when('I see 5 item’s in my to do list')
def impl(context):
  for i in range(0, 5):
    inputbox = context.browser.find_element_by_id('id_new_item')
    inputbox.send_keys('test')
    inputbox.send_keys(Keys.ENTER)
  table = context.browser.find_element_by_id('id_list_table')
  rows = table.find_elements_by_tag_name('tr')
  assert len(rows) == 5

@then('I see a “semangat, kerja keras” about how busy I am')
def impl(context):
  comment = context.browser.find_element_by_id('comment')
  assert comment.text == 'semangat, kerja keras'

@when('I see 9 item’s in my to do list')
def impl(context):
  for i in range(0, 9):
    inputbox = context.browser.find_element_by_id('id_new_item')
    inputbox.send_keys('test')
    inputbox.send_keys(Keys.ENTER)
  table = context.browser.find_element_by_id('id_list_table')
  rows = table.find_elements_by_tag_name('tr')
  assert len(rows) == 9

@then('I see a “oh tidak” about how busy I am')
def impl(context):
  comment = context.browser.find_element_by_id('comment')
  assert comment.text == 'oh tidak'
