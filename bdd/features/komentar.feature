Feature: Provide automatic comment base on the number of items
  As a user
  I want to have an automatic comment based on the number of tasks
  So I can find out how busy I am

  Scenario Outline: opening page
    Given I am opening the web page
    When I see <numberofitems> item’s in my to do list
    Then I see a <comment> about how busy I am
    Examples: comments
      | numberofitems | comment                   |
      | 0             | “yey, waktunya berlibur”  |
      | 3             | “sibuk tapi santai”       |
      | 5             | “semangat, kerja keras”   |
      | 9             | “oh tidak”                |
