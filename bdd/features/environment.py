from selenium import webdriver
from django.core import management

def before_all(context):
  context.browser = webdriver.PhantomJS()
  context.browser.set_window_size(1120, 550)
  context.browser.implicitly_wait(3)

def before_scenario(context, scenario):
  management.call_command('flush', verbosity=0, interactive=False)

def after_all(context):
  context.browser.quit()
